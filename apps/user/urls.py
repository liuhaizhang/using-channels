# !/usr/bin/python
# -*- conding: utf-8 -*-
# author: liuhaizhang
# wechat: 13510391274
from django.urls import path,include
from . import views

urlpatterns = [
    path('test/',views.TestView.as_view()),
    path('update/data/',views.UpdateUserInfo.as_view()),
]
