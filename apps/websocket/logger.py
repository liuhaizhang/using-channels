import os
import logging
from datetime import datetime, date, timedelta
from logging.handlers import RotatingFileHandler, TimedRotatingFileHandler
#输入到控制台
def _get_console_handler():
    '1、日志格式'
    formatter = logging.Formatter('[%(asctime)s][%(levelname)s][ %(funcName)s function: %(lineno)s line]:%(message)s')
    '2、输出到控制台处理器'
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(formatter)
    return console_handler

#输入日志到文件中
def _get_websocket_handler_info():
    #如果存到日志的目录不存在时，就创建该目录
    path = os.path.dirname(__file__)
    log_path = os.path.join(path, 'logs')
    if not os.path.exists(log_path):
        os.makedirs(log_path)

    # 文件名，以日期作为文件名
    log_file_name = datetime.today().strftime('%Y-%m-%d.log')
    # 构建日志文件的存储路径
    log_file_str = os.path.join(log_path, log_file_name)

    '1、日志记录的格式'
    # 默认日志等级的设置
    # logging.basicConfig(level=logging.DEBUG)
    # 设置日志的格式:发生时间,日志等级,日志信息文件名,      函数名,行数,日志信息
    formatter = logging.Formatter(
        '[%(asctime)s][%(levelname)s][%(pathname)s: %(funcName)s function: %(lineno)s line]: %(message)s')

    '2、基于时间的日志处理器'
    '''
    # 往文件里写入指定间隔时间自动生成文件的Handler
    # 实例化TimedRotatingFileHandler
    # interval是时间间隔，backupCount是备份文件的个数，如果超过这个个数，就会自动删除，when是间隔的时间单位，单位有以下几种：
    # S 秒
    # M 分
    # H 小时
    # D 天
    # 'W0'-'W6' 每星期（interval=0时代表星期一：W0）
    # midnight 每天凌晨
    '''
    file_log_time_handler = TimedRotatingFileHandler(
        filename=log_file_str,  # 日志文件的路径
        when='midnight',  # 凌晨零点进行文件分割
        backupCount=0,  # 保留旧文件数0
        interval=1,  # 分割一次
        encoding='utf-8')  # 日志处理器
    file_log_time_handler.setFormatter(formatter)  # 日志格式
    file_log_time_handler.setLevel(logging.DEBUG)  # 日志等级

    return file_log_time_handler  # 基于文件大小分割日志的方案

def _get_websocket_handler_error():
    #如果存到日志的目录不存在时，就创建该目录
    path = os.path.dirname(__file__)
    log_path = os.path.join(path, 'logs')
    if not os.path.exists(log_path):
        os.makedirs(log_path)

    # 文件名，以日期作为文件名
    log_file_name = datetime.today().strftime('%Y-%m-%d_error.log')
    # 构建日志文件的存储路径
    log_file_str = os.path.join(log_path, log_file_name)

    '1、日志记录的格式'
    # 默认日志等级的设置
    # logging.basicConfig(level=logging.DEBUG)
    # 设置日志的格式:发生时间,日志等级,日志信息文件名,      函数名,行数,日志信息
    formatter = logging.Formatter(
        '[%(asctime)s][%(levelname)s][%(pathname)s: %(funcName)s function: %(lineno)s line]: %(message)s')

    '2、基于时间的日志处理器'
    '''
    # 往文件里写入指定间隔时间自动生成文件的Handler
    # 实例化TimedRotatingFileHandler
    # interval是时间间隔，backupCount是备份文件的个数，如果超过这个个数，就会自动删除，when是间隔的时间单位，单位有以下几种：
    # S 秒
    # M 分
    # H 小时
    # D 天
    # 'W0'-'W6' 每星期（interval=0时代表星期一：W0）
    # midnight 每天凌晨
    '''
    file_log_time_handler = TimedRotatingFileHandler(
        filename=log_file_str,  # 日志文件的路径
        when='midnight',  # 凌晨零点进行文件分割
        backupCount=0,  # 保留旧文件数0
        interval=1,  # 分割一次
        encoding='utf-8')  # 日志处理器
    file_log_time_handler.setFormatter(formatter)  # 日志格式
    file_log_time_handler.setLevel(logging.DEBUG)  # 日志等级

    return file_log_time_handler  # 基于文件大小分割日志的方案

#infor日志
_info_logger = logging.getLogger('ws_log_info')
_info_logger.setLevel(logging.DEBUG)
_info_logger.addHandler(_get_websocket_handler_info())  # 添加文件日志处理器
_info_logger.addHandler(_get_console_handler())  # 添加控制台日志处理器
#导入这个使用
info_logger = _info_logger.info

#报错日志
_error_logger = logging.getLogger('ws_log_error')
_error_logger.setLevel(logging.ERROR)
_error_logger.addHandler(_get_websocket_handler_info())  # 添加文件日志处理器
_error_logger.addHandler(_get_console_handler())  # 添加控制台日志处理器
#导入这个日志
error_logger = _error_logger.error

if __name__ == '__main__':
    path = os.path.dirname(__file__)
    logs_path = os.path.join(path,'logs')
    print(logs_path)
