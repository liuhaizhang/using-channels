# !/usr/bin/python
# -*- conding: utf-8 -*-
# author: liuhaizhang
# wechat: 13510391274
from django.urls import path
from . import groupCustomers
from . import ptpCustomer

# 这个变量是存放websocket的路由
socket_urlpatterns = [
    #1、点对点通信
    path('socket/user/<str:username>/', groupCustomers.UserToUserConsumer.as_asgi()),
    path('socket/test/<str:username>/', ptpCustomer.UserToUserConsumer.as_asgi()),
    path('socket/test2/<str:username>/', ptpCustomer.UserToUserConsumerV2.as_asgi()),
    #2、群聊广播通信
    path('socket/group/<str:group>/<str:username>/', groupCustomers.GroupChatConsumer.as_asgi()),
    #3、前端数据实时更新
    path('socket/total/update/', groupCustomers.TotalDataConsumer.as_asgi()),
]