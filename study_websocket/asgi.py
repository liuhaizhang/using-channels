"""
ASGI config for study_websocket project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.2/howto/deployment/asgi/
"""


import os

from django.core.asgi import get_asgi_application
#新的模块
from channels.routing import ProtocolTypeRouter, URLRouter
# 导入chat应用中的路由模块
from apps.websocket.routings import socket_urlpatterns


#超时中间件
from channels.middleware import BaseMiddleware
class WebSocketTimeoutMiddleware(BaseMiddleware):
    def __init__(self, application, timeout=60):
        super().__init__(application)
        self.timeout = timeout

    async def __call__(self, scope, receive, send):
        scope['timeout'] = self.timeout
        return await super().__call__(scope, receive, send)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "study_websocket.settings")

application = ProtocolTypeRouter({
    # http路由走这里
    "http": get_asgi_application(),
    # chat应用下rountings模块下的路由变量socket_urlpatterns,就是存路由的列表
    "websocket": WebSocketTimeoutMiddleware(
        URLRouter(socket_urlpatterns),
        timeout = 3
    )

})
