'''
websocket-client==1.7.0
可以忽略掉SSL证书过期
'''
import websocket
from threading import Thread
import time
import json
import ssl

#当客户端接收到来自服务器的消息时，这个函数被调用
def on_message(ws, message):
    try:
        message = json.loads(message)
    except Exception:
        pass
    if isinstance(message,dict):
        msg_type = message.get('type')
        if msg_type == 'heart':
            print(f'心跳数据 {time.strftime("%Y-%m-%d %H:%M:%S")}：',message)
        elif msg_type == 'forward':
            print(f'收到转发数据{time.strftime("%Y-%m-%d %H:%M:%S")}',message)
            time.sleep(3)
            data = {
                "type":"send",
                "channel":message.get('channel'),
                "data":{
                    "time":"{}".format(time.strftime('%Y-%m-%d %H:%M:%S')),
                    "type":"热力图"
                }
            }
            ws.send(json.dumps(data,ensure_ascii=False))


#如果在WebSocket连接期间发生错误，这个函数被调用
def on_error(ws,error):
    print(f"Error: {error}")

#当WebSocket连接关闭时，这个函数被调用
def on_close(*args):
    print(args)
    print("WebSocket closed")

#发送的心跳信息
def on_ping(ws, data):
    # 在这里可以处理发送心跳消息的逻辑
    print("Sending heartbeat message: {}".format(time.strftime('%Y-%m-%d %H:%M:%S')))
    heart = {'type':'heart'}
    ws.send(json.dumps(heart))

#接收到的心跳信息
def on_pong(ws, frame_data):
    print('服务端发送过来的心跳回复',frame_data)



#运行时，就触发的函数
def on_open(ws):
    # 与服务端保持心跳
    def keep_heart(ws):
        second = 0
        while True:
            ws.send(json.dumps({'type': 'heart', 'data': "ping"}))
            # 等待一段时间再发送下一次心跳消息
            # print(second,'秒')
            # second+=5
            time.sleep(5)
    # 启动心跳线程
    heartbeat_thread = Thread(target=keep_heart, args=(ws,))
    heartbeat_thread.start()



if __name__ == "__main__":
    # WebSocket服务器的地址
    # websocket_server = "wss://yd.fiberexp.com/ws/agent/GZGS2023500005/"
    websocket_server  = "ws://localhost:8080/socket/user/lhz/"
    websocket.enableTrace(True)
    # 现在你可以使用 ws 对象来发送和接收消息
    ws = websocket.WebSocketApp(websocket_server,
                                on_ping=on_ping,#用于发送心跳数据，不写，默认是发送空字符串
                                on_pong=on_pong,#接收到心跳时，处理
                                on_message=on_message, #接收到服务端信息时触发
                                on_error=on_error, #报错触发的
                                on_close=on_close, #关闭触发的
                                header={"Token":"xxxxxx"},#携带上请求头信息，token
                                # on_open=on_open #运行就触发
                                )

    # 启动客户端
    ws.run_forever(
        sslopt={"cert_reqs": ssl.CERT_NONE},#ssl证书过期时，可以设置忽略证书
    )
