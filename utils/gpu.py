import asyncio
import websocket as websockets


async def ws_client():
    async with websockets.connect('ws://example.com/websocket') as websocket:
        # 发送消息
        await websocket.send('Hello, Server!')

        # 接收消息
        response = await websocket.recv()
        print(f'Received message: {response}')

        # 持续收发消息
        while True:
            message = input('Enter a message to send: ')
            await websocket.send(message)
            response = await websocket.recv()
            print(f'Received response: {response}')


# 运行客户端
asyncio.get_event_loop().run_until_complete(ws_client())