import os
import logging
from datetime import datetime, date, timedelta
from logging.handlers import RotatingFileHandler, TimedRotatingFileHandler

# 项目根目录
BASE_DIR = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
# 日志文件存放的目录
LOGS_DIR = os.path.join(BASE_DIR, 'logs')
if not os.path.exists(LOGS_DIR):
    os.makedirs(LOGS_DIR)

#输入到控制台
def get_console_handler():
    '1、日志格式'
    formatter = logging.Formatter('[%(asctime)s][%(levelname)s][ %(funcName)s function: %(lineno)s line]:%(message)s')
    '2、输出到控制台处理器'
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(formatter)
    return console_handler

#输入日志到文件中
def get_websocket_handler():
    # 文件名，以日期作为文件名
    log_file_name = datetime.today().strftime('%Y-%m-%d.log')
    # 构建日志文件的路径l
    log_file_str = os.path.join(LOGS_DIR, log_file_name)

    '1、日志记录的格式'
    # 默认日志等级的设置
    # logging.basicConfig(level=logging.DEBUG)
    # 设置日志的格式:发生时间,日志等级,日志信息文件名,      函数名,行数,日志信息
    formatter = logging.Formatter(
        '[%(asctime)s][%(levelname)s][%(pathname)s: %(funcName)s function: %(lineno)s line]: %(message)s')

    '2、基于时间的日志处理器'
    '''
    # 往文件里写入指定间隔时间自动生成文件的Handler
    # 实例化TimedRotatingFileHandler
    # interval是时间间隔，backupCount是备份文件的个数，如果超过这个个数，就会自动删除，when是间隔的时间单位，单位有以下几种：
    # S 秒
    # M 分
    # H 小时
    # D 天
    # 'W0'-'W6' 每星期（interval=0时代表星期一：W0）
    # midnight 每天凌晨
    '''
    file_log_time_handler = TimedRotatingFileHandler(
        filename=log_file_str,  # 日志文件的路径
        when='midnight',  # 凌晨零点进行文件分割
        backupCount=0,  # 保留旧文件数0
        interval=1,  # 分割一次
        encoding='utf-8')  # 日志处理器
    file_log_time_handler.setFormatter(formatter)  # 日志格式
    file_log_time_handler.setLevel(logging.DEBUG)  # 日志等级

    return file_log_time_handler  # 基于文件大小分割日志的方案

ws_logger = logging.getLogger('ws_log')
ws_logger.setLevel(logging.DEBUG)
ws_logger.addHandler(get_websocket_handler())  # 添加文件日志处理器
ws_logger.addHandler(get_console_handler())  # 添加控制台日志处理器
